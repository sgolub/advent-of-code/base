# Advent of Code Golang Framework

[Advent of Code](http://www.adventofcode.com) is an annual coding challenge and story leading up to Christmas.

## Installation

```bash
go install gitlab.com/sgolub/advent-of-code/base/v2/cmd/advent
```

## Usage

Once it's installed, you have the `advent` command available:

```bash
advent cli command

Usage:
  advent [command]

Available Commands:
  help        Help about any command
  install     installs the specified day(s)
  new         generates a new module for the day
  run

Flags:
  -p, --data-path string   The base path for the data to be stored on disk (default "code/.aoc_days")
  -h, --help               help for advent

Use "advent [command] --help" for more information about a command.
```

### Scaffold a new Day

This command will allow you to create a new folder for the specified day. It's default behavior is to assume the current year, however you can override this.

When you run `advent new #`, it looks for the root folder `advent-of-code`. This will then create a folder `day-#` under the year specified.

```
advent-of-code
├── 2020
│   ├── day-1
│   │   ├── README.md
│   │   ├── data.yaml
│   │   ├── go.mod
│   │   ├── go.sum
│   │   ├── main.go
│   │   └── sample.yaml
```

This will download the prompt for that day (and convert it) to `README.md`, create an empty `data.yaml` and `sample.yaml` that should ultimately contain your datasets, and generate the `main.go` file that should contain your solutions.

Additionally, `go mod init` and `git init` will be run.

### Compiling/Installing a Day

When you're ready to test your solution, you can run the `advent install` command in the same folder as your module. If you want to automatically run it after compilation, add the `--run` flag. See the `--help` for more options.

### Running Previously Compiled Days

Whenever you compile a day via the `install` command, it will move the dataset files and the compile plugin module (`.so`) to the specified data folder, which defaults to `~/code/.aoc_days`. This means you can run all, or one of any day from a year.
