package main

import (
	"os"

	"gitlab.com/sgolub/advent-of-code/base/v2/cmd/advent/commands"
)

func main() {
	err := commands.ExecuteRoot()
	if err != nil {
		os.Exit(1)
	}
}
