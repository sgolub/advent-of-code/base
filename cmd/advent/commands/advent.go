package commands

import (
	"gitlab.com/sgolub/advent-of-code/base/v2/days"

	"github.com/spf13/cobra"
)

var (
	advent *cobra.Command = func() *cobra.Command {
		var baseDataPath *string
		c := &cobra.Command{
			Use:   "advent",
			Short: "advent cli command",
			PersistentPreRun: func(*cobra.Command, []string) {
				days.Control().SetBasePath(*baseDataPath)
			},
		}

		c.AddCommand(runCommand)
		c.AddCommand(installCommand)
		c.AddCommand(newCommand)

		baseDataPath = c.PersistentFlags().StringP("data-path", "p", "code/.aoc_days", "The base path for the data to be stored on disk")

		return c
	}()
)

// ExecuteRoot will execute the root advent command
func ExecuteRoot() error {
	return advent.Execute()
}
