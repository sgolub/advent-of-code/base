package commands

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"

	"github.com/spf13/cobra"
)

var (
	installCommand = func() *cobra.Command {
		var andRun *bool
		var useSample *bool
		var solutionNumber *int
		var data *[]string
		c := &cobra.Command{
			Use:   "install",
			Short: "installs the specified day(s)",
			RunE: func(_ *cobra.Command, args []string) error {
				if len(args) == 0 {
					d, err := os.Getwd()
					if err != nil {
						return fmt.Errorf("failed to get current directory: %s", err)
					}
					args = []string{d}
				}
				var dayList []days.Day
				for _, d := range args {
					d, err := filepath.Abs(d)
					if err != nil {
						return fmt.Errorf("failed to get abs path for %s", d)
					}
					day, err := days.Control().Install(d)
					if err != nil {
						return fmt.Errorf("failed to install day: %s", err)
					}
					dayList = append(dayList, day)
				}
				if *andRun && len(dayList) > 0 {
					for _, day := range dayList {
						runSolution(day, *solutionNumber, *useSample, *data)
					}
				}
				return nil
			},
		}

		andRun = c.PersistentFlags().BoolP("run", "r", false, "run the day after it is installed")
		useSample = c.PersistentFlags().BoolP("sample", "s", false, "use sample if running solution after install")
		solutionNumber = c.PersistentFlags().IntP("solution", "i", 0, "which solution should we run. defaults to all")
		data = c.PersistentFlags().StringArray("data", []string{}, "any additional data to be sent to the run flag in key=val format")

		return c
	}()
)
