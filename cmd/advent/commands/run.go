package commands

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"

	"github.com/spf13/cobra"
)

var (
	runSolution = func(day days.Day, solutionNumber int, useSample bool, data []string) error {
		fmt.Printf("Day %d-%d\n", day.Year(), day.Day())
		for _, d := range data {
			splits := strings.Split(d, "=")
			if len(splits) == 2 {
				day.SetData(splits[0], splits[1])
			} else {
				day.SetData(splits[0], "true")
			}
		}
		if solutionNumber == 0 || solutionNumber == 1 {
			fmt.Printf("Solution #%d\n", 1)
			fmt.Println(day.Solution1(useSample))
		}
		if solutionNumber == 0 || solutionNumber == 2 {
			fmt.Printf("Solution #%d\n", 2)
			fmt.Println(day.Solution2(useSample))
		}
		return nil
	}
	runCommand *cobra.Command = func() *cobra.Command {
		var yearNumber *int
		var dayNumber *int
		var useSample *bool
		var solutionNumber *int
		var data *[]string

		c := &cobra.Command{
			Use: "run",
			RunE: func(_ *cobra.Command, _ []string) error {
				if yearNumber == nil || *yearNumber == 0 {
					*yearNumber, _, _ = time.Now().Date()
				}
				if *dayNumber == 0 {
					allDays, err := days.Control().GetAll()
					if err != nil {
						return fmt.Errorf("failed to load all days: %s", err)
					}
					for _, d := range allDays {
						err := runSolution(d, *solutionNumber, *useSample, *data)
						if err != nil {
							return fmt.Errorf("failed to run day %d: %s", *dayNumber, err)
						}
					}
				} else {
					d, err := days.Control().Get(*yearNumber, *dayNumber)
					if err != nil {
						return fmt.Errorf("failed to load day: %s", err)
					}
					return runSolution(d, *solutionNumber, *useSample, *data)
				}
				return nil
			},
		}

		yearNumber = c.PersistentFlags().IntP("year", "y", 0, "the year to choose the day from. defaults to this year")
		dayNumber = c.PersistentFlags().IntP("day", "d", 0, "the day that you wish to run. defaults to all")
		useSample = c.PersistentFlags().BoolP("use-sample", "s", false, "should we use sample data. defaults to no")
		solutionNumber = c.PersistentFlags().IntP("solution", "i", 0, "which solution should we run. defaults to all")
		data = c.PersistentFlags().StringArray("data", []string{}, "any additional data to be sent to the run flag in key=val format")

		return c
	}()
)
