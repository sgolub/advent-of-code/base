package commands

import (
	"strconv"
	"time"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"

	"github.com/spf13/cobra"
)

var (
	newCommand = func() *cobra.Command {
		var year *int
		c := &cobra.Command{
			Use:   "new",
			Short: "generates a new module for the day",
			Args:  cobra.ExactArgs(1),
			RunE: func(_ *cobra.Command, args []string) error {
				day, err := strconv.Atoi(args[0])
				if err != nil {
					return err
				}
				return days.Control().New(*year, day)
			},
		}

		thisYear, _, _ := time.Now().Date()

		year = c.PersistentFlags().IntP("year", "y", thisYear, "the year to create the day")

		return c
	}()
)
