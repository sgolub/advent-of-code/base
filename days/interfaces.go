package days

type Day interface {
	Day() int
	Year() int
	Solution1(bool) string
	Solution2(bool) string
	SetData(key, value string)
}

type PuzzleImplementation interface {
	Solution()
}
