package days

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"plugin"
	"strconv"
	"strings"
	"time"

	"github.com/hoisie/mustache"
	"github.com/lunny/html2md"
	"golang.org/x/net/html"
	"gopkg.in/yaml.v2"
)

const (
	buildPluginCommand = "go build -buildmode=plugin -o %s"
	dataSampleFileName = "sample_%d_%d.yaml"
	dataFileName       = "data_%d_%d.yaml"
	moduleFileName     = "day_%d_%d.so"
	modPath            = "gitlab.com/sgolub/advent-of-code/base/v2/%d/day-%d"
)

type dayControl struct {
	modules      []module
	pathFromHome string
}

type templateContext struct {
	DayNumber  int
	ModuleBase string
	YearNumber int
}

type module string

var (
	ctrl                       = &dayControl{}
	dayFiles map[string]string = map[string]string{
		"data.yaml":      "",
		"sample.yaml":    "",
		"README.md":      "https://adventofcode.com/{{ YearNumber }}/day/{{ DayNumber }}",
		"main.go":        "./assets/day.go.template",
		".gitlab-ci.yml": "./assets/day-gl-ci.yml",
	}
)

// TimeTrack will track the execution time.
func TimeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Printf("%s took %s\n", name, elapsed)
}

// NewTimer will create a new timer to track execution time.
func NewTimer(name string) {
	TimeTrack(time.Now(), name)
}

func (m module) Path() string {
	return string(m)
}

func (m module) String() string {
	return m.Path()
}

func (m module) fileName() string {
	p := m.Path()
	return strings.TrimSuffix(filepath.Base(p), filepath.Ext(p))
}

func (m module) Day() int {
	splits := strings.Split(m.fileName(), "_")
	i, _ := strconv.Atoi(splits[2])
	return i
}

func (m module) Year() int {
	splits := strings.Split(m.fileName(), "_")
	i, _ := strconv.Atoi(splits[1])
	return i
}

func (m module) Load() (Day, error) {
	if _, err := os.Stat(m.Path()); os.IsNotExist(err) {
		return nil, fmt.Errorf("module path does not exist: %s", m)
	}
	plug, err := plugin.Open(m.Path())
	if err != nil {
		return nil, fmt.Errorf("failed to open path: %s", err)
	}

	symPlug, err := plug.Lookup("GetDay")
	if err != nil {
		return nil, fmt.Errorf("failed to find instance of day: %s", err)
	}

	newDay, ok := symPlug.(func() Day)
	if !ok {
		return nil, fmt.Errorf("unexpected day type: %T", symPlug)
	}
	return newDay(), nil
}

// Control is the days management system
func Control() *dayControl {
	return ctrl
}

func (d *dayControl) SetBasePath(path string) {
	d.pathFromHome = path
}

func (d *dayControl) modulePath() string {
	home, _ := os.UserHomeDir()
	return filepath.Join(home, d.pathFromHome)
}

func (d *dayControl) loadModulePaths() error {
	p := d.modulePath()

	d.modules = []module{}
	err := filepath.Walk(p, func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) == ".so" {
			d.modules = append(d.modules, module(path))
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("failed to load modules: %s", err)
	}
	return nil
}

func (d *dayControl) getModules() []module {
	if d.modules == nil || len(d.modules) == 0 {
		d.loadModulePaths()
	}
	return d.modules
}

func (d *dayControl) Get(year, day int) (Day, error) {
	for _, m := range d.getModules() {
		if m.Year() == year && m.Day() == day {
			return m.Load()
		}
	}
	return nil, fmt.Errorf("failed to load day %d", day)
}

func (d *dayControl) GetAll() ([]Day, error) {
	days := []Day{}

	for _, m := range d.getModules() {
		d, err := m.Load()
		if err != nil {
			return nil, fmt.Errorf("failed to load module: %s", err)
		}
		days = append(days, d)
	}
	return days, nil
}

func (d *dayControl) getDataFileName(day Day, useSample bool) string {
	var filename string
	if useSample {
		filename = dataSampleFileName
	} else {
		filename = dataFileName
	}
	return fmt.Sprintf(filename, day.Year(), day.Day())
}

func (d *dayControl) LoadData(day Day, useSample bool) map[interface{}]interface{} {
	home, _ := os.UserHomeDir()
	fp := filepath.Join(home, d.pathFromHome, d.getDataFileName(day, useSample))
	strData, err := ioutil.ReadFile(fp)
	if err != nil {
		fmt.Printf("Error loading Data: %s\n", err)
		return nil
	}
	results := make(map[interface{}]interface{})
	err = yaml.Unmarshal(strData, &results)
	if err != nil {
		fmt.Printf("Error Parsing Data: %s\n", err)
		return nil
	}
	return results
}

func getYearAndDayFromPath(path string) (year, day int, err error) {
	sep := string(filepath.Separator)
	splits := strings.Split(strings.TrimSuffix(path, sep), sep)
	year, err = strconv.Atoi(splits[len(splits)-2])
	if err != nil {
		return 0, 0, fmt.Errorf("failed to parse year: %s", err)
	}
	daySplit := strings.Split(splits[len(splits)-1], "-")
	day, err = strconv.Atoi(daySplit[1])
	if err != nil {
		return 0, 0, fmt.Errorf("failed to parse day: %s", err)
	}
	return year, day, nil
}

func copyFile(src, dest string) error {
	srcStat, err := os.Stat(src)
	if os.IsNotExist(err) {
		return nil
	}

	if !srcStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", src)
	}

	srcFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	destFile, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, srcFile)
	return err
}

func (d *dayControl) Install(path string) (Day, error) {
	year, day, err := getYearAndDayFromPath(path)
	if err != nil {
		return nil, fmt.Errorf("failed to extract year and day from path: %s", err)
	}
	fileName := filepath.Join(d.modulePath(), fmt.Sprintf(moduleFileName, year, day))
	cmdParts := strings.Split(fmt.Sprintf(buildPluginCommand, fileName), " ")
	cmd := exec.Command(cmdParts[0], cmdParts[1:]...)
	cmd.Dir = path
	var out bytes.Buffer
	cmd.Stderr = &out
	err = cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("failed to build module to path: %s", out.String())
	}
	err = copyFile(filepath.Join(path, "sample.yaml"), filepath.Join(d.modulePath(), fmt.Sprintf(dataSampleFileName, year, day)))
	if err != nil && !os.IsNotExist(err) {
		return nil, fmt.Errorf("failed to copy sample data: %s", err)
	}
	err = copyFile(filepath.Join(path, "data.yaml"), filepath.Join(d.modulePath(), fmt.Sprintf(dataFileName, year, day)))
	if err != nil && !os.IsNotExist(err) {
		return nil, fmt.Errorf("failed to copy data: %s", err)
	}
	return d.Get(year, day)
}

func (d *dayControl) Exists(year, day int) bool {
	all, _ := d.GetAll()
	for _, d := range all {
		if d.Year() == year && d.Day() == day {
			return true
		}
	}
	return false
}

func git(args ...string) (string, error) {
	cmd := exec.Command("git", args...)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return "", err
	}
	return out.String(), nil
}

func commitFiles(fileList []string, message string) error {
	for _, fileName := range fileList {
		_, err := git("add", fileName)
		if err != nil {
			return err
		}
	}
	gitArgs := []string{"commit", "-m", message}
	gitArgs = append(gitArgs, fileList...)
	_, err := git(gitArgs...)
	return err
}

func findAOCBase() (string, error) {
	d, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("problem finding cwd: %s", err)
	}
	splits := strings.Split(d, string(filepath.Separator))
	path := ""
	for i := len(splits) - 1; i >= 0; i-- {
		if splits[i] == "advent-of-code" {
			path = filepath.Join(splits[:i+1]...)
			break
		}
	}
	if path == "" {
		return "", errors.New("failed to locate base advent-of-code folder")
	}
	if !strings.HasPrefix(path, string(filepath.Separator)) {
		path = fmt.Sprintf("%s%s", string(filepath.Separator), path)
	}
	return path, nil
}

func buildFileList() []string {
	files := make([]string, 0)
	for fileName := range dayFiles {
		files = append(files, fileName)
	}
	return files
}

func (d *dayControl) Latest(year int) int {
	maxDay := 0
	all, _ := d.GetAll()
	for _, day := range all {
		if day.Year() != year {
			continue
		}
		if day.Day() > maxDay {
			maxDay = day.Day()
		}
	}
	return maxDay
}

func ReadURLToMarkdown(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("Unable to download day: %s", err)
	}
	doc, err := html.Parse(resp.Body)
	if err != nil {
		return "", fmt.Errorf("Unable to parse HTML: %s", err)
	}
	var f func(*html.Node)
	var content *html.Node
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "article" {
			for _, a := range n.Attr {
				if a.Key == "class" && strings.Contains(a.Val, "day-desc") {
					content = n
					return
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)

	if content == nil {
		return "", fmt.Errorf("Failed to locate article text")
	}
	strContent := renderNode(content)
	mdContent := html2md.Convert(strContent)
	return strings.TrimSpace(mdContent), nil
}

func renderNode(n *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, n)
	return buf.String()
}

func getModulePath() (string, error) {
	aocBase, err := findAOCBase()
	if err != nil {
		return "", fmt.Errorf("failed to find base dir: %w", err)
	}
	fp := filepath.Join(aocBase, "base", "go.mod")
	strData, err := ioutil.ReadFile(fp)
	if err != nil {
		return "", fmt.Errorf("failed to load go.mod file: %w", err)
	}
	for _, line := range strings.Split(string(strData), "\n") {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "module") {
			return strings.Split(line, " ")[1], nil
		}
	}
	return "", fmt.Errorf("failed to locate module directive")
}

func (d *dayControl) New(year, day int) error {
	if d.Exists(year, day) {
		return fmt.Errorf("Day %d-%d already exists", year, day)
	}
	fmt.Printf("Creating new module for day %d-%d\n", year, day)
	baseModPath, err := getModulePath()
	if err != nil {
		return fmt.Errorf("failed to locate module path: %w", errors.Unwrap(err))
	}
	ctx := &templateContext{
		YearNumber: year,
		DayNumber:  day,
		ModuleBase: baseModPath,
	}
	aocBase, err := findAOCBase()
	if err != nil {
		return err
	}
	fmt.Println("Creating Folder for Day", day)
	dayFolder := filepath.Join(aocBase, strconv.Itoa(year), fmt.Sprintf("day-%d", day))
	err = os.MkdirAll(dayFolder, 0755)
	if err != nil {
		return fmt.Errorf("failed to create module folder: %s", err)
	}
	err = os.Chdir(dayFolder)
	if err != nil {
		return fmt.Errorf("failed to change to module folder: %s", err)
	}
	for filePath, templatePath := range dayFiles {
		fmt.Println("Creating", filePath)
		f, err := os.Create(filePath)
		if err != nil {
			return err
		}
		defer f.Close()
		if templatePath != "" {
			fmt.Println("Writing data to", filePath)
			var fileData string
			if strings.HasPrefix(templatePath, "https") {
				fileData, err = ReadURLToMarkdown(mustache.Render(templatePath, ctx))
				if err != nil {
					fileData = ""
				}
			} else {
				templatePath = filepath.Join(aocBase, "base", templatePath)
				fileData = mustache.RenderFile(templatePath, ctx)
			}
			_, err = f.WriteString(fileData)
			if err != nil {
				return err
			}
		}
	}
	mPath := fmt.Sprintf(modPath, year, day)
	fmt.Println("Initializing go module", mPath)
	cmd := exec.Command("go", "mod", "init", mPath)
	cmd.Dir = dayFolder
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Failed init", string(out))
		return err
	}
	_, err = git("init")
	if err != nil {
		return err
	}
	err = commitFiles(buildFileList(), fmt.Sprintf("Adding in base for day %d-%d", year, day))
	if err != nil {
		return err
	}
	return nil
}
