module gitlab.com/sgolub/advent-of-code/base/v2

go 1.15

require (
	github.com/apaxa-go/eval v0.0.0-20171223182326-1d18b251d679
	github.com/apaxa-go/helper v0.0.0-20180607175117-61d31b1c31c3 // indirect
	github.com/beefsack/go-astar v0.0.0-20171024231011-f324bbb0d6f7 // indirect
	github.com/hoisie/mustache v0.0.0-20160804235033-6375acf62c69
	github.com/jmoiron/jigo v0.0.0-20140813022801-f75f9d0842df
	github.com/kindermoumoute/adventofcode v0.0.0-20181223174905-af6c67068a44
	github.com/lunny/html2md v0.0.0-20181018071239-7d234de44546
	github.com/mattn/go-runewidth v0.0.7 // indirect
	github.com/mattn/godown v0.0.0-20180312012330-2e9e17e0ea51
	github.com/spf13/cobra v1.0.0
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools/gopls v0.5.0 // indirect
	gopkg.in/yaml.v2 v2.2.7
)
